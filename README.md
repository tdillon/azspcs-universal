# Universal

Generic project to run all contest solvers with common Firebase backend.

## Adding New Contests

Create a new folder within `src`, e.g., `src/some-contest-name`.
Create a file named `worker.ts` within that new folder.
The `name` returned by the `WORKING` message must match the folder name.

## Local Authentication

In order to run locally, Node must be able to authenticate.
Save your Google cloud token to `.key.json`.
Then execute the following to allow Node to authenticate.

```shell
export GOOGLE_APPLICATION_CREDENTIALS="./.key.json"
```
