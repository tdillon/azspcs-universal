export class Hex {
    private _n: number;
    private _numRows: number;
    private _rowLength: number[];
    private _solution: number[][];
    private _MAGIC_NUMBER = 666;
    private _potentialLayers: Array<Map<number, number[][]>> = [];

    /**
     * @param s AZSPCS solution formatted string to use to create a Hex object.
     * e.g., (1,1,2),(3,4,5,6),(7,3,4,3,1),(2,3,4,5),(6,7,1)
     */
    constructor(s: string);

    /**
     * @param n Size of Hex to create and initialize to all zeroes.
     */
    constructor(n: number);

    /**
     * Creates a new hex that has an "n" value 1 more than the h.n.
     * The new hex is initiated to all zeros, then h is centered in the new hex.
     * The outer layer of h is not included in the new hex.
     * 
     * @param h A hex used has the base for new hex that is 1 sizes larger.
     */
    constructor(h: Hex);

    constructor(x: (string | number | Hex)) {
        this._solution = [];

        for (let i = 0; i < 8; ++i) {
            this._potentialLayers[i] = new Map<number, number[][]>();
        }

        if (typeof x === 'number') {
            this.n = + x;
            this.initDataStructure();
        } else if (typeof x === 'string') {
            this.n = x.substring(0, x.indexOf(')')).replace(/\D/g, '').length;
            x = x.replace(/\s*/g, '');  // Get rid of space characters.
            this.initDataStructure();
            this.setCells(x);
        } else { // assume Hex
            this.n = x.n + 1;
            this.initDataStructure();
            for (let i = 2; i < this._numRows - 2; ++i) {
                for (let j = 2; j < this._rowLength[i] - 2; ++j) {
                    this.setCell(i, j, x.getCell(i - 1, j - 1));
                }
            }
        }
    }

    set n(val: number) {
        this._n = val;
        this._numRows = this._n * 2 - 1;
        this._rowLength = [];
        for (let i = 0; i < this._numRows; ++i) {
            this._rowLength[i] = i <= (this._n * 2 - 1) / 2 ? i + this._n : this._n * 2 - 1 - (i + 1) + this._n;
        }
    }

    get n() {
        return this._n;
    }

    get numRows() {
        return this._numRows;
    }

    getRowLength(i: number) {
        return this._rowLength[i];
    }

    initDataStructure() {
        for (let i = 0; i < this._numRows; ++i) {
            this._solution[i] = Array(this._rowLength[i]).fill(0);
        }
    }

    /**
     * @param hexStr Must be formatted the same as Hex.toString().  Any spaces or new lines most likely bomb this.
     */
    setCells(hexStr: string) {
        let strIdx = -1;

        for (let i = 0, rowLen = this._rowLength[i]; i < this._numRows; rowLen = this._rowLength[++i]) {
            for (let j = 0; j < rowLen; ++j) {
                while (Number.isNaN(+hexStr[++strIdx]));
                this._solution[i][j] = +hexStr[strIdx];
            }
        }
    }

    getCell(i: number, j: number) {
        return this._solution[i] ? this._solution[i][j] : undefined;
    }

    setCell(i: number, j: number, val: number) {
        return this.getCell(i, j) !== undefined && val > 0 && val < 8 ? this._solution[i][j] = val : undefined;
    }

    /**
     * Returns an interger that uses 4 bit segments to represent the neighbors at i, j.
     * The order of cells is (from least to most significant) NW, NE, W, E, SW, SE.
     * If a neighbor can have no value (e.g. NW, NE, and W for 0,0) then 15 (i.e., 1111) is used.
     * The 8 most significant bits are not used.
     *
     * @example
     *   1 2
     *  3 N 4
     *   5 6
     * -> 6636321 i.e., 0000|0000|0110|0101|0100|0011|0010|0001
     *
     * @example
     *  N 1
     * 3 2
     * -> 2301951 i.e., 0000|0000|0010|0011|0001|1111|1111|1111
     *
     * @param {number} i The row into Hex#_solution
     * @param {number} j The column into Hex#_solution[i]
     * @returns {number} The 32 bit representation of the 6 neighbors of the cell at i,j.
     */
    getNeighbors(i: number, j: number): number {
        let n32 = 0;

        // NW & NE
        if (i === 0) {  // top row
            n32 |= 255;
        } else {
            if (i < this._n) {  // From the top to the middle row
                n32 |= (j === 0 ? 15 : this._solution[i - 1][j - 1]);
                n32 |= ((j === this._rowLength[i] - 1 ? 15 : this._solution[i - 1][j]) << 4);
            } else {  // After the middle row
                n32 |= this._solution[i - 1][j];
                n32 |= (this._solution[i - 1][j + 1] << 4);
            }
        }

        // W
        n32 |= ((j === 0 ? 15 : this._solution[i][j - 1]) << 8);

        // E
        n32 |= ((j === this._rowLength[i] - 1 ? 15 : this._solution[i][j + 1]) << 12);

        // SW & SE
        if (i === this._numRows - 1) {  //bottom row
            n32 |= (255 << 16);
        } else {
            if (i < this._n - 1) {  // From the top to the middle row (not including the middle row)
                n32 |= (this._solution[i + 1][j] << 16);
                n32 |= (this._solution[i + 1][j + 1] << 20);
            } else {  // The middle row and below
                n32 |= ((j === 0 ? 15 : this._solution[i + 1][j - 1]) << 16);
                n32 |= ((j === this._rowLength[i] - 1 ? 15 : this._solution[i + 1][j]) << 20);
            }
        }

        return n32;
    }

    setNeighbors(i: number, j: number, neighbors: number) {
        if (i > 0) {  // NW & NE are not set for top row.
            if (i < this._n) {
                if (j > 0 && (neighbors & 15) !== 0) {
                    this._solution[i - 1][j - 1] = (neighbors & 15);  // NW
                }
                if (j < this._rowLength[i] - 1 && ((neighbors >> 4) & 15) !== 0) {
                    this._solution[i - 1][j] = ((neighbors >> 4) & 15);  // NE
                }
            } else {
                if ((neighbors & 15) !== 0) {
                    this._solution[i - 1][j] = (neighbors & 15);  // NW
                }
                if (((neighbors >> 4) & 15) !== 0) {
                    this._solution[i - 1][j + 1] = ((neighbors >> 4) & 15);  // NE
                }
            }
        }

        if (j > 0 && ((neighbors >> 8) & 15) !== 0) {  // W is not set for left most cells.
            this._solution[i][j - 1] = ((neighbors >> 8) & 15);  // W
        }

        if (j < this._rowLength[i] - 1 && ((neighbors >> 12) & 15) !== 0) {  // E is not set for right most cells.
            this._solution[i][j + 1] = ((neighbors >> 12) & 15);  // E
        }

        if (i < this._numRows - 1) {  // SW & SE are not set for bottom row.
            if (i < this._n - 1) {
                if (((neighbors >> 16) & 15) !== 0) {
                    this._solution[i + 1][j] = ((neighbors >> 16) & 15);  // SW
                }
                if (((neighbors >> 20) & 15) !== 0) {
                    this._solution[i + 1][j + 1] = ((neighbors >> 20) & 15);  // SE
                }
            } else {
                if (j > 0 && ((neighbors >> 16) & 15) !== 0) {
                    this._solution[i + 1][j - 1] = ((neighbors >> 16) & 15);  // SW
                }
                if (j < this._rowLength[i] - 1 && ((neighbors >> 20) & 15) !== 0) {
                    this._solution[i + 1][j] = ((neighbors >> 20) & 15);  // SE
                }
            }
        }
    }

    hasValidStructure() {
        if (!this._solution || !this._solution.length || this._solution.length !== this._numRows) {
            return false;
        }

        if (this._solution.some((row, i) => this._rowLength[i] !== row.length)) {
            return false;
        }

        return true;
    }

    isValid() {
        return this._solution.every((arr, i) => arr.every((value, j) => this.hasValidNeighbors(i, j)));
    }

    hasValidNeighbors(i: number, j: number) {
        let k = this._solution[i][j];
        let neighbors = this.getNeighbors(i, j);
        let n: number;

        for (let index = 1; index < k; ++index) {
            for (n = 0; ((neighbors >> (n * 4)) & 15) !== index && n < 6; ++n);

            if (n === 6) {
                return false;
            }
        }

        return true;
    }

    getPotentialLayers(i: number, j: number, hexPerms: Set<number>[], neighbors: number) {
        const potentialLayers: number[][] = [null, [], [], [], [], [], [], []];
        const cell = this._solution[i][j];

        for (let k = cell === 0 ? 1 : cell; k <= cell || (cell === 0 && k <= 7); ++k) {
            for (let item of hexPerms[k].values()) {
                let good = true;

                for (let m = 0; good && m < 6; ++m) {
                    good = ((item >> (4 * m)) & 15) === 0 ||
                        ((neighbors >> (4 * m)) & 15) === 0 ||
                        ((item >> (4 * m)) & 15) === ((neighbors >> (4 * m)) & 15);
                }

                if (good) {
                    potentialLayers[k].push(item);
                }
            }
        }

        return potentialLayers;
    }



    foo(x: Set<number>[]) {
        let maxI = 0;  // Keep track of the best progress
        let maxHexStr: string;  // The best partial
        let cell: number;
        let pl: number[][];
        let plN: number;
        let randIdx2: number;
        let neighbors: number;

        // TODO check for the following optimization
        /**
         * If cell !== 0, and none of my neighbors are 0, i have no reason to setNeighbors, right?
         */
        for (let i = 0, numIters = 0; i < this._numRows && numIters < this._n * this._MAGIC_NUMBER; ++i, ++numIters) {  // TODO: this probably shouldn't be linear
            for (let j = 0; j < this._rowLength[i]; ++j) {
                neighbors = this.getNeighbors(i, j);
                cell = this._solution[i][j];

                pl = this._potentialLayers[cell].get(neighbors);
                if (!pl) {
                    pl = this.getPotentialLayers(i, j, x, neighbors);
                    this._potentialLayers[cell].set(neighbors, pl);
                }

                if (cell === 0) {
                    // TODO: use a non-uniformly distributed random number and a 1d array potential layers.  but how to handle what cell value?
                    plN = 8;
                    while (pl[--plN].length === 0) ;
                    randIdx2 = Math.floor(Math.random() * pl[plN].length);
                    this._solution[i][j] = plN;
                    this.setNeighbors(i, j, pl[plN][randIdx2]);
                } else {
                    if (pl[cell].length === 0) {
                        // Set i-1, i, and i+1 to 0 and start from the beginning of the hex.
                        if (i > maxI || !maxHexStr) {
                            let i2;
                            for (i2 = i === this._numRows - 1 ? i : i + 1; i2 > i - 2 && i2 >= 0; --i2) {
                                for (let j2 = 0; j2 < this._rowLength[i2]; ++j2) {
                                    this._solution[i2][j2] = 0;
                                }
                            }
                            maxI = i;
                            maxHexStr = this.toString();
                        } else {
                            this.setCells(maxHexStr);
                        }
                        i = -1;
                        j = Number.MAX_SAFE_INTEGER;
                        continue;
                    } else {
                        this.setNeighbors(i, j, pl[cell][Math.floor(Math.random() * pl[cell].length)]);
                    }
                }
            }
        }
    }

    setToZeros() {
        for (let i = 0, rowLen = this._rowLength[i]; i < this._numRows; rowLen = this._rowLength[++i]) {
            for (let j = 0; j < rowLen; ++j) {
                this._solution[i][j] = 0;
            }
        }
    }

    getScore() {
        let score = 0;

        if (this.isValid()) {
            score = this._solution.reduce((previous, current) => previous + current.reduce((prev, curr) => prev + curr, 0), 0);
        }

        return score - (3 * Math.pow(this.n, 2) - 3 * this.n + 1);
    }

    /**
     * @example
     * .toString()
     * (2,2,2),(3,2,3,3),(2,1,3,3,1),(2,3,3,2),(3,1,1)
     * 
     * @example
     * toString(false)
     * ┏━━━━━━━━━━━┓
     * ┃ n:3  s:45 ┃
     * ┣━━━━━━━━━━━┫
     * ┃   2 2 2   ┃
     * ┃  3 2 3 3  ┃
     * ┃ 2 1 3 3 1 ┃
     * ┃  2 3 3 2  ┃
     * ┃   3 1 3   ┃
     * ┗━━━━━━━━━━━┛
     *
     * @example
     * .toString(true)
     * ┏━━━━━━━━━━━┓
     * ┃ n:3  s:45 ┃
     * ┣━━━━━━━━━━━┫
     * (   2,2,2   ),
     * (  3,2,3,3  ),
     * ( 2,1,3,3,1 ),
     * (  2,3,3,2  ),
     * (   3,1,3   )
     * ┗━━━━━━━━━━━┛
     * @param prettySubmit If true, return a pretty box representation of the solution including size and score.  If false or omitted, return the AZSPCS solution formatted string.
     */
    toString(prettySubmit?: boolean) {
        if (typeof prettySubmit === 'boolean') {
            const score = this.getScore().toString();
            /** Width of the widest row in the hex.  For n ==3, this would be 9.  Does not include padding. */
            const maxWidth = this._rowLength[this._n - 1] * 2 - 1;
            /** The total number of spaces (left + right) wider than the widest row in the hex. */
            const padding = 2;

            const prettyArray = [
                // ┏━━━━━━━━━━━┓
                '┏',
                '━'.repeat(maxWidth + padding),
                '┓',
                '\n',
                // ┃ n:3  s:45 ┃
                '┃ n:',
                this._n.toString(),
                ' '.repeat(maxWidth - 4 - this._n.toString().length - score.length),
                's:',
                score,
                ' ┃',
                '\n',
                // ┣━━━━━━━━━━━┫
                '┣',
                '━'.repeat(maxWidth + padding),
                '┫',
                '\n',
                // ┃   2 2 2   ┃
                // or
                // (   2,2,2   ),
                ...this._solution.map((row, i) => [
                    prettySubmit ? '(' : '┃',
                    ' '.repeat(Math.floor(((maxWidth + 1) - this._rowLength[i] * 2) / 2) + 1),
                    row.join(prettySubmit ? ',' : ' '),
                    ' '.repeat(Math.ceil(((maxWidth + 1) - this._rowLength[i] * 2) / 2) + 1),
                    prettySubmit ? ')' : '┃',
                    prettySubmit && i !== this._numRows - 1 ? ',' : '',
                    '\n',
                ].join('')),
                // ┗━━━━━━━━━━━┛
                '┗',
                '━'.repeat(maxWidth + padding),
                '┛',
            ];
            return prettyArray.join('');
        } else {
            return `(${this._solution.join("),(")})`;
        }
    }
}
