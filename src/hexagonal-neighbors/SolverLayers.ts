import { Hex } from "./Hex.js";

/**
 * @example
 * const s = new SolverLayers(3, 57, new UniqueHexPermutation().hexPerms);
 * s.onNewBest = s.onNewInfo = str => console.log(str);
 * s.solve();
 */
export class SolverLayers {
    private hex: Hex;

    onNewBest = (bestHex: string, score:number): void => {
        throw Error("You must implement onNewBest.");
    };

    // TODO rename x
    constructor(n: number, private knownBest: number, private x: Set<number>[]) {
        this.hex = new Hex(n);
    }

    solve() {
        let tempScore: number;

        while (true) {
            do {
                this.hex.setToZeros();
                this.hex.foo(this.x);

                tempScore = this.hex.getScore();

            } while (tempScore === 0);

            if (tempScore > this.knownBest) {
                this.knownBest = tempScore;
                this.onNewBest(this.hex.toString(), tempScore);
            }
        }
    }
}
