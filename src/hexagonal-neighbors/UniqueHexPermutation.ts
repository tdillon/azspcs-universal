export class UniqueHexPermutation {
    /** TODO: Explain what this thing is. */
    hexPerms: Set<number>[];
    private allInputs = [
        [0, 0, 0, 0, 0, 0],
        [1, 0, 0, 0, 0, 0],
        [1, 2, 0, 0, 0, 0],
        [1, 2, 3, 0, 0, 0],
        [1, 2, 3, 4, 0, 0],
        [1, 2, 3, 4, 5, 0],
        [1, 2, 3, 4, 5, 6]
    ];

    constructor() {  // https://stackoverflow.com/questions/9960908/permutations-in-javascript
        let result: number[][];
        let set: Set<number>;

        this.hexPerms = [null];

        const permute = (arr: number[], m: number[] = []) => {
            if (arr.length === 0) {
                result.push(m)
            } else {
                for (let i = 0; i < arr.length; i++) {
                    let current = arr.slice();
                    let next = current.splice(i, 1);
                    permute(current.slice(), m.concat(next))
                }
            }
        }

        this.allInputs.forEach((i, idx) => {
            result = [];
            set = new Set();
            permute(i);

            result.forEach(
                permArray => set.add(
                    permArray.reduce(
                        (prev, curr, currIdx, arr) =>  prev |= curr << (4 * currIdx), 0
                    )
                )
            );

            this.hexPerms.push(set);
        });
    }
}
