import { Firestore, DocumentReference } from "@google-cloud/firestore";

type tgdData = { solution: string; score: number; }
interface tgdDocData extends FirebaseFirestore.DocumentData, tgdData { }
type tgdDocRef = FirebaseFirestore.DocumentReference<tgdDocData>;
export type tgdSaveResponse = { solution: string, score: number, message: string, lastUpdate: number }

export interface FirestoreError {
  // I cannot find this any any package for typings, so I'll make my own.
  // https://firebase.google.com/docs/reference/js/firebase.firestore.FirestoreError
  code: "cancelled" | "unknown" | "invalid-argument" | "deadline-exceeded" | "not-found" | "already-exists" | "permission-denied" | "resource-exhausted" | "failed-precondition" | "aborted" | "out-of-range" | "unimplemented" | "internal" | "unavailable" | "data-loss" | "unauthenticated";
  message: string;
  name: string;
  stack?: string;
}

const db = new Firestore({ projectId: 'azspcs-universal' });

export function getSolution(contestName: string, problemName: string) {
  return (<tgdDocRef>db.collection(contestName).doc(problemName)).get();
}

export function getSolutions(contestName: string) {
  return <Promise<FirebaseFirestore.QuerySnapshot<tgdDocData>>>db.collection(contestName).get();
}

/**
 * 
 * @param contestName 
 * @param problemName 
 * @param solution 
 * @param score 
 * @param scoreTester TODO explain what this does!!!
 * @returns {Promise<string>} The string returned in the promise should be `JSON.parse` to return a `tgdSaveResponse` object.
 */
export function saveSolution(
  contestName: string,
  problemName: string,
  solution: string,
  score: number,
  scoreTester: (scoreToCheck: number, scoreFromDB: number) => boolean) {

  let problemRef = <tgdDocRef>db.collection(contestName).doc(problemName);

  return db.runTransaction(async t => {
    const doc = await t.get(problemRef);
    let docData = doc.data();

    if (!docData || scoreTester(score, docData.score)) {
      t.set<tgdData>(doc.ref, { score, solution });

      const saveResponse: tgdSaveResponse = {
        message: `[DAO] Solution saved with score [${score}] improves upon previous score [${docData ? docData.score : -1}]`,
        score,
        solution,
        lastUpdate: doc.readTime.toMillis() // updateTime is null, use readTime to have a reasonable value
      };

      return Promise.resolve(JSON.stringify(saveResponse));
    } else {
      return Promise.reject<FirestoreError>(<FirestoreError>{
        code: "out-of-range",
        name: 'Where is this shown?',
        message: `[DAO] New solution with score [${score}] does not improve upon existing score [${docData.score}]`
      });
    }
  });
}
