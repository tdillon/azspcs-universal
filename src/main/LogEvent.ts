export default class {
    private _date: Date;

    constructor(private _message: string) {
        this._date = new Date();
    }

    /**
     * @example
     * const l = new L('foo');
     * l.datetime // e.g., '2020-03-13 13:12:22'
     */
    get datetime() { return `${this._date.toISOString().substring(0, 10)} ${this._date.toTimeString().substring(0, 8)}` }

    /**
     * @example
     * const l = new L('foo');
     * l.message // e.g., 'foo'
     */
    get message() { return this._message }
}