import express, { Request, Response } from 'express';
import bodyParser from 'body-parser';
import { getSolution, getSolutions, saveSolution, FirestoreError, tgdSaveResponse } from "./DAO";

type APIError = {
    status: 'ERROR';
    message: string;
}

type APIProblemData = {
    name: string;
    score: number;
    solution: string;
    when: number;
}

export type APISaveResponse = {
    status: 'SUCCESS';
    message: string;
    contest: string;
    problem: string;
    score: number;
    solution: string;
    lastUpdate: number;
} | APIError;

export type APISaveBodyData = {
    solution: string;
    score: number;
    minIsBest: boolean;
}

export type APIGetSolutionResponse = {
    status: 'SUCCESS';
    message: string;
    solution: APIProblemData;
} | APIError;

export type APIGetProblemsResponse = {
    status: 'SUCCESS';
    message: string;
    problems: APIProblemData[];
} | APIError;

const port = process.env.PORT || 8080;
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('dist-web/main'));
app.use(express.static('dist-web', { index: false }));

app.get('/:c/:p', (req, res) => {
    const contestName: string = req.params.c;
    const problemName: string = req.params.p;

    getSolution(contestName, problemName)
        .then(s => {
            const solution = s.data();

            if (solution) {
                res.send(<APIGetSolutionResponse>{ status: 'SUCCESS', message: `[SERVER] Solution found for ${contestName} problem ${problemName}`, solution });
            } else {
                res.send(<APIGetSolutionResponse>{ status: 'ERROR', message: `[SERVER] [ERROR] No solution found for ${contestName} problem ${problemName}` });
            }
        })
        .catch((e: FirestoreError) => {
            res.send(<APIGetSolutionResponse>{
                status: 'ERROR',
                message: `[SERVER] [ERROR] getSolution: ${e.message}`
            });
        });
});

app.get('/:c', (req, res) => {
    const contestName: string = req.params.c;

    getSolutions(contestName)
        .then(s => {
            const problems: APIProblemData[] = s.docs.map(ss => {
                return {
                    name: ss.id,
                    score: ss.data().score,
                    solution: ss.data().solution,
                    when: ss.updateTime.toMillis()
                }
            });

            if (problems) {
                res.send(<APIGetProblemsResponse>{ status: 'SUCCESS', message: `[SERVER] Solutions found for ${contestName}`, problems });
            } else {
                res.send(<APIGetProblemsResponse>{ status: 'ERROR', message: `[SERVER] [ERROR] No solutions found for ${contestName}` });
            }
        })
        .catch((e: FirestoreError) => res.send(<APIGetProblemsResponse>{
            status: 'ERROR',
            message: `[SERVER] [ERROR] getSolutions: ${e.message}`
        }));
});


app.post('/:c/:p', (req: Request, res: Response) => {
    const contestName: string = req.params.c;
    const problemName: string = req.params.p;
    const body: APISaveBodyData = req.body;
    const solution = body.solution;
    const score = body.score;
    const minIsBest = body.minIsBest;
    const comparatorFunction: (a: number, b: number) => boolean = (a: number, b: number) => minIsBest ? a < b : a > b;

    saveSolution(contestName, problemName, solution, score, comparatorFunction)
        .then((msg: string) => {
            const saveResponse = <tgdSaveResponse>JSON.parse(msg);
            res.send(<APISaveResponse>{
                status: 'SUCCESS',
                message: saveResponse.message,
                contest: contestName,
                problem: problemName,
                score: saveResponse.score,
                solution: saveResponse.solution,
                lastUpdate: saveResponse.lastUpdate
            })
        })
        .catch((e: FirestoreError) => {
            res.send(<APIError>{ status: 'ERROR', message: e.message })
        });
});

app.listen(port, () => console.log(`Node (${process.version}) Express app listening on port ${port}!`))
