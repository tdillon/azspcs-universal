import Grid from './Grid.js';
import { BrowserMessageEvent, WorkerMessageData } from "../main/browser";

const CONTEST_NAME = 'reversing-nearness';

const ctx: Worker = self as any; // HACK from https://github.com/Microsoft/TypeScript/issues/20595#issuecomment-390359040

const knownBests = [5526, 17779, 57152, 144459, 362950, 740798, 1585264, 2888120, 5457848, 9164700, 15891088, 25152826, 40901354, 61784724, 95115180, 138133813, 203877974, 286960950, 409173438, 560363762, 776271362, 1039341134, 1404785310, 1843328926, 2439441116];

// Send 'working' message back to browser, signifies that browser supports modules in web workers.
ctx.postMessage(<WorkerMessageData>{
    type: 'WORKING',
    message: `[WORKER] worker for ${CONTEST_NAME} is functional`,
    name: CONTEST_NAME,
    problems: Array.from({ length: 25 }, (v, i) => {
        return { name: (i + 6).toString(), knownBest: knownBests[i] }
    })
});

onmessage = (e: BrowserMessageEvent) => {
    const browserMessageData = e.data;
    ctx.postMessage(<WorkerMessageData>{
        type: 'LOG',
        message: `[WORKER] ${CONTEST_NAME} received ${browserMessageData.type} message`
    });

    switch (browserMessageData.type) {
        case 'SOLVE':
            ctx.postMessage(<WorkerMessageData>{
                type: 'LOG',
                message: `[WORKER] ${CONTEST_NAME} received request to solve problem ${browserMessageData.problem} with best ${browserMessageData.myBest}`
            });

            let g = new Grid(+browserMessageData.problem);
            let bestScore = browserMessageData.myBest || Number.MAX_SAFE_INTEGER;

            while (true) {
                g.getGreedyBest();
                if (g.score < bestScore) {
                    bestScore = g.score;

                    let x: WorkerMessageData = {
                        type: 'SOLUTION',
                        message: `[WORKER] new solution for ${CONTEST_NAME} problem ${browserMessageData.problem} with score ${g.score}`,
                        contest: CONTEST_NAME,
                        problem: browserMessageData.problem,
                        solution: g.toString(),
                        score: g.score,
                        comparator: '<'
                    };
                    ctx.postMessage(x);
                }
                g.randomize();
            }

            break;
        default:
            ctx.postMessage(<WorkerMessageData>{
                type: 'LOG',
                message: `[WORKER] [ERROR] Bad message type: ${browserMessageData.type}`
            });
            break;
    }
}
