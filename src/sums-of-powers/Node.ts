import Solution from "./Solution"

export default class T {
    static counter = 0;
    id: number;
    e:bigint
    left: T
    right: T
    isComplete = false;
    isBad = false;
    maxPotential = 0n; // If all child bases are used, what is the largest value?

    constructor(public n: number, public value: bigint) {
        this.id = ++T.counter;
        if (n === 1) {
            this.isComplete = true;
        }
    }
}