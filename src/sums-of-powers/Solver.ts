import Node from "./Node";
import Solution from "./Solution";

export default class {
    private _bases: number[];
    private _root: Node;
    _solution: Solution;

    constructor(private _problem: number, private _s: number, private _best: bigint) {
        this._root = new Node(this._s, Solution.power(this._s, this._problem));
        this._root.e = this._root.value;
    }

    public get solution(): Solution {
        return this._solution;
    }

    foo() {
        this._solution = new Solution(this._problem, this._s);

        let _cur = this._root;

        do {
            let sop = _cur.e - Solution.sumOfPowers(_cur.n - 1, this._problem);
            if (sop > (this._best < 0 ? -this._best : this._best)) { // my max e is worse than best
                _cur.isBad = true;
            } else if (!(_cur.left || _cur.right)) {  // Both children are null, pick random.
                let newNode = new Node(_cur.n - 1, Solution.power(_cur.n - 1, this._problem));

                if (Math.random() >= .5) {  // LEFT
                    _cur.left = newNode;
                    newNode.e = _cur.e;
                } else { // RIGHT
                    _cur.right = newNode;
                    newNode.e = _cur.e - newNode.value;

                    if (!this._solution.addBase(newNode.n)) {  // Adding this base does not improve the score
                        newNode.isBad = true;
                    }
                }

                _cur = newNode;
            } else if (_cur.left && _cur.right) {  // Both children are not null, pick good random.
                let goLeft = Math.random() >= .5;
                let parent = _cur;

                if (goLeft && !(_cur.left.isBad || _cur.left.isComplete)) {  // LEFT
                    _cur = _cur.left;
                } else if (!goLeft && !(_cur.right.isBad || _cur.right.isComplete)) { // RIGHT
                    _cur = _cur.right;

                    if (!this._solution.addBase(_cur.n)) {  // Adding this base does not improve the score
                        _cur.isBad = true;
                    }
                }

                if ((parent.left.isComplete || parent.left.isBad) && (parent.right.isComplete || parent.right.isBad)) {
                    parent.isComplete = true;
                }
            } else if (!_cur.left) {  // Left is null, always choose the null child (TODO is that a good idea?)
                _cur.left = new Node(_cur.n - 1, Solution.power(_cur.n - 1, this._problem));
                _cur.left.e = _cur.e;

                if ((_cur.left.isComplete || _cur.left.isBad) && (_cur.right.isComplete || _cur.right.isBad)) {
                    _cur.isComplete = true;
                }

                _cur = _cur.left;
            } else {  // Right is null
                let parent = _cur;
                _cur.right = new Node(_cur.n - 1, Solution.power(_cur.n - 1, this._problem));
                _cur.right.e = _cur.e - _cur.right.value;
                _cur = _cur.right;

                if (!this._solution.addBase(_cur.n)) {  // Adding this base does not improve the score
                    _cur.isBad = true;
                }

                if ((parent.left.isComplete || parent.left.isBad) && (parent.right.isComplete || parent.right.isBad)) {
                    parent.isComplete = true;
                }
            }
        } while (_cur.n > 1 && !(_cur.isBad || _cur.isComplete));

        if (this._solution.isValid && this._solution.absE < (this._best < 0 ? -this._best : this._best)) {
            this._best = this._solution.E;
        }

        return this._solution.isValid && this._best === this.solution.E;
    }


    bar(): string {
        /**
         * graph TD
         * A --> B
         * A --> C
         *
         * B --> 12
         * B --> 21
         *
         * C --> E
         * C --> F
         *
         */

        let mermaid = ['graph TB', 'classDef bad fill:#f00', 'classDef complete fill:#0f0']

        this.bar1(this._root, mermaid);

        return mermaid.join('\n');
    }

    private bar1(n: Node, s: string[]): void {
        if (!n) return;

        if (n.left) {
            s.push(`${n.id}[s:${n.n}<br>#${n.id}<br>v:${n.value}<br>E:${n.e}<br>complete:${n.isComplete}] --> |L| ${n.left.id}[s:${n.left.n}<br>#${n.left.id}<br>v:${n.left.value}<br>E:${n.left.e}<br>complete:${n.left.isComplete}]`);
            if (n.left.isBad) {
                s.push(`class ${n.left.id} bad;`);
            }
        }

        if (n.right) {
            s.push(`${n.id}[s:${n.n}<br>#${n.id}<br>v:${n.value}<br>E:${n.e}<br>complete:${n.isComplete}] --> |R| ${n.right.id}[s:${n.right.n}<br>#${n.right.id}<br>v:${n.right.value}<br>E:${n.right.e}<br>complete:${n.right.isComplete}]`);
            if (n.right.isBad) {
                s.push(`class ${n.right.id} bad;`);
            }
        }

        if (n.isComplete) {
            s.push(`class ${n.id} complete;`);
        }

        this.bar1(n.left, s);

        this.bar1(n.right, s)
    }


    toString() {
        return `${this._s}^${this._problem}=>{${this._bases.join(',')}}`;
    }
}
